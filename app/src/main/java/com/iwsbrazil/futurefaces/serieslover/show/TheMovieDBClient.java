package com.iwsbrazil.futurefaces.serieslover.show;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by mschuh on 18/09/16.
 */
public class TheMovieDBClient {

    private final String API_KEY = "19a6e422fdf1d8733bbebbab4245ca8c";
    private final String API_BASE_URL = "http://api.themoviedb.org/3/";
    private AsyncHttpClient client;

    public TheMovieDBClient() {
        this.client = new AsyncHttpClient();
    }

    private String getApiUrl(String relativeUrl) {
        return API_BASE_URL + relativeUrl;
    }

    public void getPopularTVShows(JsonHttpResponseHandler handler) {
        String url = getApiUrl("discover/tv?");
        RequestParams params = new RequestParams("api_key", API_KEY);
        params.add("sort_by", "popularity.desc");
        client.get(url, params, handler);
    }

    public void getTVShowTrailer(String id, JsonHttpResponseHandler handler) {
        String url = getApiUrl("tv/" + id + "/videos?id=" + id);
        RequestParams params = new RequestParams("api_key", API_KEY);
        //client.get(url, handler);
        client.get(url, params, handler);
    }
}
