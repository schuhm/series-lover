package com.iwsbrazil.futurefaces.serieslover.ui.util;


/**
 * Created by administrador on 15/09/16.
 */
public final class NavigationMode {
    public static final int GRID = 0;
    public static final int LIST = 1;
}
