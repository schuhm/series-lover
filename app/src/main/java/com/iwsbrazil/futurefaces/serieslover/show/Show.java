package com.iwsbrazil.futurefaces.serieslover.show;

import android.os.Parcel;
import android.os.Parcelable;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by administrador on 15/09/16.
 */
public class Show implements Parcelable {
    private String id;
    private String title;
    private Double score;
    private int thumbnail;
    private String overview;
    private String posterPath;
    private String backdropPath;
    private Date airDate;
    private String trailerUrl;

    public Show() {
    }

    public Show(String id, String title, Double score, int thumbnail, String overview, String posterPath, String backdropPath, Date airDate) {
        this.id = id;
        this.title = title;
        this.score = score;
        this.thumbnail = thumbnail;
        this.overview = overview;
        this.posterPath = posterPath;
        this.backdropPath = backdropPath;
        this.airDate = airDate;
    }

    // Returns a Show given the expected JSON
    // Show.fromJson(showJsonDictionary)
    public static Show fromJson(JSONObject jsonObject) {
        Show s = new Show();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            // Deserialize json into object fields
            s.id = jsonObject.getString("id");
            s.title = jsonObject.getString("name");
            s.score = jsonObject.getDouble("vote_average");
            s.overview = jsonObject.getString("overview");
            s.posterPath = jsonObject.getString("poster_path");
            s.backdropPath = jsonObject.getString("backdrop_path");
            s.airDate = formatter.parse(jsonObject.getString("first_air_date"));
            fetchTrailerUrl(s);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Return new object
        return s;
    }

    private static void fetchTrailerUrl (final Show show) {
        TheMovieDBClient client = new TheMovieDBClient();

        client.getTVShowTrailer(show.getId(), new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray items = null;
                try {
                    // Get the movies json array
                    items = responseBody.getJSONArray("results");
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject item = null;
                        try {
                            item = items.getJSONObject(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                            continue;
                        }
                        if (item.has("key")) {
                            show.setTrailerUrl(item.get("key").toString());
                            break;
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    // Decodes array of shows json results into show model objects
    // Show.fromJson(jsonArrayOfShows)
    public static ArrayList<Show> fromJson(JSONArray jsonArray) {
        ArrayList<Show> shows = new ArrayList<Show>(jsonArray.length());
        // Process each result in json array, decode and convert to movie
        // object
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject showsJson = null;
            try {
                showsJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Show show = Show.fromJson(showsJson);
            if (show != null) {
                shows.add(show);
            }
        }

        return shows;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Date getAirDate() {
        return airDate;
    }

    public void setAirDate(Date airDate) {
        this.airDate = airDate;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPosterPath() {
        return "http://image.tmdb.org/t/p/w500/" + posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBackdropPath() {
        return "http://image.tmdb.org/t/p/w500/" +  backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }



    protected Show(Parcel in) {
        id = in.readString();
        title = in.readString();
        score = in.readByte() == 0x00 ? null : in.readDouble();
        thumbnail = in.readInt();
        overview = in.readString();
        posterPath = in.readString();
        backdropPath = in.readString();
        long tmpAirDate = in.readLong();
        airDate = tmpAirDate != -1 ? new Date(tmpAirDate) : null;
        trailerUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        if (score == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(score);
        }
        dest.writeInt(thumbnail);
        dest.writeString(overview);
        dest.writeString(posterPath);
        dest.writeString(backdropPath);
        dest.writeLong(airDate != null ? airDate.getTime() : -1L);
        dest.writeString(trailerUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Show> CREATOR = new Parcelable.Creator<Show>() {
        @Override
        public Show createFromParcel(Parcel in) {
            return new Show(in);
        }

        @Override
        public Show[] newArray(int size) {
            return new Show[size];
        }
    };
}