package com.iwsbrazil.futurefaces.serieslover.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;

import com.iwsbrazil.futurefaces.serieslover.ui.util.NavigationMode;
import com.iwsbrazil.futurefaces.serieslover.R;
import com.iwsbrazil.futurefaces.serieslover.show.Show;
import com.iwsbrazil.futurefaces.serieslover.show.TheMovieDBClient;
import com.iwsbrazil.futurefaces.serieslover.ui.helpers.SimpleItemTouchHelperCallback;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ShowAdapter adapter;
    private List<Show> showList;
    private RecyclerView.LayoutManager gridLayoutManager;
    private RecyclerView.LayoutManager listLayoutManager;
    private TheMovieDBClient client;
    private int navigationMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        showList = new ArrayList<>();
        adapter = new ShowAdapter(this, showList);

        gridLayoutManager = new GridLayoutManager(this,2);
        listLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(gridLayoutManager);
        setNavigationMode(NavigationMode.GRID);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        fetchShows();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_nav_mode:
                switch (getNavigationMode()) {
                    case NavigationMode.GRID:
                        item.setIcon(getDrawable(R.drawable.ic_view_grid_white_48dp));
                        setNavigationMode(NavigationMode.LIST);
                        recyclerView.setLayoutManager(listLayoutManager);
                        break;

                    case NavigationMode.LIST:
                        item.setIcon(getDrawable(R.drawable.ic_view_list_white_48dp));
                        setNavigationMode(NavigationMode.GRID);
                        recyclerView.setLayoutManager(gridLayoutManager);
                        break;

                }
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void fetchShows() {
        client = new TheMovieDBClient();
        client.getPopularTVShows(new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray items = null;
                try {
                    // Get the movies json array
                    items = responseBody.getJSONArray("results");
                    // Parse json array into array of model objects
                    ArrayList<Show> shows = Show.fromJson(items);
                    // Load model objects into the adapter
                    for (Show s : shows) {
                        showList.add(s); // add show to showList
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public int getNavigationMode() {
        return navigationMode;
    }

    public void setNavigationMode(int navigationMode) {
        this.navigationMode = navigationMode;
    }

}

