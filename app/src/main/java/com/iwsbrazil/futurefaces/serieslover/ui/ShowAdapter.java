package com.iwsbrazil.futurefaces.serieslover.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iwsbrazil.futurefaces.serieslover.ui.util.NavigationMode;
import com.iwsbrazil.futurefaces.serieslover.R;
import com.iwsbrazil.futurefaces.serieslover.show.Show;
import com.iwsbrazil.futurefaces.serieslover.ui.helpers.ItemTouchHelperAdapter;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import static android.support.v4.app.ActivityOptionsCompat.makeSceneTransitionAnimation;

/**
 * Created by administrador on 15/09/16.
 */
public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.MyViewHolder> implements ItemTouchHelperAdapter {

    private Context mContext;
    private List<Show> showList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, score;
        public ImageView thumbnail;

        public MyViewHolder (View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            score = (TextView) view.findViewById(R.id.score);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }

    // Provide a suitable constructor
    public ShowAdapter(Context mContext, List<Show> showList) {
        this.mContext = mContext;
        this.showList = showList;
    }


    @Override
    public int getItemViewType(int position) {
        if (this.mContext instanceof MainActivity) {
            MainActivity main = (MainActivity) this.mContext;
            return main.getNavigationMode();
        }
        return -1;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case NavigationMode.GRID:
                itemView = inflater.inflate(R.layout.show_grid, parent, false);
                break;
            case NavigationMode.LIST:
                itemView = inflater.inflate(R.layout.show_list, parent, false);
                break;
            default:
                itemView = inflater.inflate(R.layout.show_grid, parent, false);
                break;
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Show show = showList.get(position);
        final ImageView thumbnail = holder.thumbnail;
        final TextView title = holder.title;
        final TextView score = holder.score;

        title.setText(show.getTitle());
        score.setText(show.getScore().toString());

        // loading show cover using Picasso library
        Picasso.with(mContext).load(show.getPosterPath()).into(thumbnail);

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ShowDetailActivity.class);
                // Pass data object in the bundle and populate details activity.
                intent.putExtra(ShowDetailActivity.EXTRA_MESSAGE, show);
                ActivityOptionsCompat options =
                        makeSceneTransitionAnimation((Activity) mContext, thumbnail, "profile");
                mContext.startActivity(intent, options.toBundle());
            }
        });
    }

    @Override
    public void onItemDismiss(int position) {
        showList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(showList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(showList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }


    @Override
    public int getItemCount() {
        return showList.size();
    }


}


