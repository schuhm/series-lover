package com.iwsbrazil.futurefaces.serieslover.ui;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iwsbrazil.futurefaces.serieslover.R;
import com.iwsbrazil.futurefaces.serieslover.show.Show;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ShowDetailActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.iwsbrazil.futurefaces.serieslover.SHOWDETAIL";
    private Toolbar actionBarToolbar;
    private CollapsingToolbarLayout collapsingToolbar;
    private TextView overview;
    private TextView score;
    private TextView airDate;
    private ImageView thumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final Show show = intent.getExtras().getParcelable(EXTRA_MESSAGE);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(show.getTitle());

        overview = (TextView) findViewById(R.id.overview);
        overview.setText(show.getOverview());

        score = (TextView) findViewById(R.id.score);
        score.setText(show.getScore().toString());

        airDate = (TextView) findViewById(R.id.air_date);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        airDate.setText(getResources().getString(R.string.release_date, formatter.format(show.getAirDate())) );

        thumbnail = (ImageView) findViewById(R.id.thumbnail);
        Picasso.with(this).load(show.getPosterPath()).into(thumbnail);

        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.trailer);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                watchYoutubeVideo(show.getTrailerUrl());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sample_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // your logic
                return true;
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void watchYoutubeVideo(String id){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://www.youtube.com/watch?v=" + id));
            startActivity(intent);
        }
    }
}
